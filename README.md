# Sprint planning helper

This program finds the most efficient set of tasks to be taken from the pool of groomed tasks.The script takes two arguments (first: a path to csv with the following columns: task_id, story_points, KSP; second: velocity of the team) and it prints comma-separated task_ids that were chosen for the sprint and a newline at the end.

### Prerequisites

You need two libraries to use this program: sys and cvs. They are included in The Python Standard Library.

### Installing

To run the program you need two arguments: filename and velocity. Filename is a name of csv file with date and velocity is a number. Example:

```
sprint-planning-helper.py csv_with_tasks.csv 13
```
