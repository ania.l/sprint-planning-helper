#!/usr/bin/python

import sys
import csv

if len(sys.argv) != 3:
    print("Please input 2 arguments: filename and velocity.")
    exit()

def check_files():
    '''Checks exist the file.
    '''
    try:
        open(sys.argv[1])
    except:
        print("File "+sys.argv[1]+" does not exist")
        exit()

def check_velocity():
    """Checks whether is a number.
    """
    try:
        float(sys.argv[2])
    except:
        print("Wrong arguments: please input velocity as a number")
        exit()
class Sprint:
    '''Bo to z klasa to zawsze lepiej.
    '''
    def __init__(self, csvname):
        with open(csvname, 'r') as csvinput:
            self.file_contents = list(csv.reader(csvinput, delimiter=','))
            self.name = self.file_contents[0]
            self.file_contents.pop(0)
            self.task_id = []
            self.story_pts = []
            self.KSP = []
            self.value = []
            for row in range(len(self.file_contents)):
                self.task_id.append(self.file_contents[row][0])
                self.story_pts.append(float(self.file_contents[row][1]))
                self.KSP.append(float(self.file_contents[row][2]))
                self.value.append(self.story_pts[-1]/self.KSP[-1])

    def sort_value(self):
        '''Sorts values of the data according to value.
        '''
        self.task_id = [task for __,task in sorted(zip(self.value,self.task_id))]
        self.story_pts = [pts for __,pts in sorted(zip(self.value,self.story_pts))]
        self.KSP = [ksp for __,ksp in sorted(zip(self.value,self.KSP))]
        self.value = sorted(self.value)
    def limit_story_pts(self,velocity):
        '''Choose the set of the best tasks.
        '''
        total_story_pts = 0
        for i in range(len(self.story_pts)):
            total_story_pts += self.story_pts[i]
            if total_story_pts <= velocity:
                sys.stdout.write(self.task_id[i]+", ")
            else:
                sys.stdout.write("\b\b \n")
                break
if __name__=="__main__":
    check_files()
    check_velocity()
    sprint = Sprint(sys.argv[1])
    sprint.sort_value()
    sprint.limit_story_pts(float(sys.argv[2]))
